﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StairBlocker : MonoBehaviour {

    [Tooltip("Values for enabled movement. 0 for enable all, 1 for enable forward, -1 for enable backward")]
    public Vector3 MovementEnabled;

    private GameObject Player;
    private Vector3 lastPlayer;

    private void Start()
    {
        lastPlayer = new Vector3(0, 0, 0);
    }

    // Use this for initialization
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Player = other.gameObject;
            lastPlayer = Player.transform.position;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Player = null;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject == Player)
        {
            float xMovement = lastPlayer.x - Player.transform.position.x;
            float yMovement = lastPlayer.y - Player.transform.position.y;
            float zMovement = lastPlayer.z - Player.transform.position.z;

            //ha a nulla felé megy az a pozitív

            Vector3 newPos = lastPlayer;
            if (xMovement > 0 && MovementEnabled.x >= 0)
            {
                newPos.x = Player.transform.position.x;
            }
            else if (xMovement < 0 && MovementEnabled.x <= 00)
            {
                newPos.x = Player.transform.position.x;
            }

            if (yMovement > 0 && MovementEnabled.y >= 0)
            {
                newPos.y = Player.transform.position.y;
            }
            else if (yMovement < 0 && MovementEnabled.y <= 00)
            {
                newPos.y = Player.transform.position.y;
            }

            if (zMovement > 0 && MovementEnabled.z >= 0)
            {
                newPos.z = Player.transform.position.z;
            }
            else if (zMovement < 0 && MovementEnabled.z <= 00)
            {
                newPos.z = Player.transform.position.z;
            }

            Player.transform.position = newPos;
            lastPlayer = newPos;
        }
    }



}
