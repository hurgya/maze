﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorEnablerScript : MonoBehaviour {

    public bool Enabler;
    public Collider collider;
    // Use this for initialization

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (Enabler)
                collider.enabled = false;
            else
                collider.enabled = true;
        }
    }

}
