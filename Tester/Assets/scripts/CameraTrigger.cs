﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTrigger : MonoBehaviour {

    public GameObject WallSegment;
    public Camera camera;

    // Use this for initialization
    bool IsCameraActive;
 

    private void Start()
    {
        IsCameraActive = false;
    }

    private void Update()
    {
        if (IsCameraActive)
        {
            camera.transform.rotation = Camera.main.transform.rotation;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            IsCameraActive = true;
            camera.enabled = true;
            WallSegment.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            IsCameraActive = false;
            camera.enabled = false;
            WallSegment.SetActive(false);
        }
    }
}
