﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour {

    public Vector3 TeleportTo;
	// Use this for initialization

    public void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.tag);
        if (other.gameObject.tag == "Player")
        {
            Vector3 newPos;
            if (TeleportTo.x == 0)
                newPos.x = other.transform.position.x;
            else
                newPos.x = TeleportTo.x;

            if (TeleportTo.y == 0)
                newPos.y = other.transform.position.y;
            else
                newPos.y = TeleportTo.y;

            if (TeleportTo.z == 0)
                newPos.z = other.transform.position.z;
            else
                newPos.z = TeleportTo.z;

            other.transform.position = newPos;
        }
    }
}
