﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStat : MonoBehaviour {


    private Vector3 Force;
    private float MaxSpeed;
    private float AddForce;
    private Camera camera;

    public float speedH = 2.0f;
    public float speedV = 2.0f;

    private float yaw = 0.0f;
    private float pitch = 0.0f;


    private Rigidbody rigid;

	// Use this for initialization
	void Start ()
    {
        Force = new Vector3(0, 0, 0);
        MaxSpeed = 50;
        AddForce = 10;
        rigid = gameObject.GetComponent<Rigidbody>();
        camera = gameObject.GetComponentInChildren<Camera>();
	}

    private void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            rigid.AddForce(transform.forward * AddForce);
        }
        else if (Input.GetKey(KeyCode.S))
        {
            rigid.AddForce(-transform.forward * AddForce);
        }

        if (Input.GetKey(KeyCode.D))
        {
            rigid.AddForce(transform.right * AddForce);
        }
        else if (Input.GetKey(KeyCode.A))
        {
            rigid.AddForce(-transform.right * AddForce);
        }

        if (rigid.velocity.x > MaxSpeed)
        {
            rigid.AddForce(MaxSpeed - rigid.velocity.x, 0, 0);
        }
        else if (rigid.velocity.x < -MaxSpeed)
        {
            rigid.AddForce(-(rigid.velocity.x + MaxSpeed), 0, 0);
        }

        if (rigid.velocity.y > MaxSpeed)
        {
            rigid.AddForce(0, MaxSpeed - rigid.velocity.y, 0);
        }
        else if (rigid.velocity.y < -MaxSpeed)
        {
            rigid.AddForce(0, -(rigid.velocity.y + MaxSpeed), 0);
        }

        yaw += speedH * Input.GetAxis("Mouse X");
        pitch -= speedV * Input.GetAxis("Mouse Y");

        transform.eulerAngles = new Vector3(0.0f, yaw, 0.0f);

    }
}
